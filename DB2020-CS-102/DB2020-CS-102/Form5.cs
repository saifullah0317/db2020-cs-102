﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Assessment values(@Title,@DateCreated,@TotalMarks,@TotalWeightage)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Title", textBox1.Text);
            cmd.Parameters.AddWithValue("@DateCreated", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@TotalMarks", textBox4.Text);
            cmd.Parameters.AddWithValue("@TotalWeightage", textBox3.Text);


            cmd.ExecuteNonQuery();
            MessageBox.Show("Assessment Details Added Successfully!!!!!");
            this.Hide();
            Form5 form = new Form5();
            form.Show();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 form = new Form5();
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment where id=@id", con);
            cmd.Parameters.AddWithValue("@id", textBox2.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update Assessment set DateCreated=@date,TotalMarks=@marks,TotalWeightage=@weightage where Title=@title", con);
            cmd.Parameters.AddWithValue("@title", textBox1.Text);
            cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@marks", int.Parse(textBox4.Text));
            cmd.Parameters.AddWithValue("@weightage", int.Parse(textBox3.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Assessments Updated Successfully");
            this.Hide();
            Form5 form = new Form5();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title,DateCreated,TotalMarks,TotalWeightage from Assessment where Title=@title", con);
            cmd.Parameters.AddWithValue("@title", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox1.Text = da.GetValue(0).ToString();
                dateTimePicker1.Text = da.GetValue(1).ToString();
                textBox4.Text = da.GetValue(2).ToString();
                textBox3.Text = da.GetValue(3).ToString();
            }
            if (textBox1.Text == "")
            {
                textBox1.Clear();
                textBox3.Clear();
                textBox4.Clear();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (Assessment components, Students' evaluation, etc.) regarding this Assessment will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From Assessment a,AssessmentComponent ac WHERE a.Id=ac.AssessmentId and ac.Id=StudentResult.AssessmentComponentId and a.Title=@t and a.DateCreated=@d", con);
                cmd.Parameters.AddWithValue("@t", textBox1.Text);
                cmd.Parameters.AddWithValue("@d", Convert.ToDateTime(dateTimePicker1.Text));
                cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("DELETE AssessmentComponent From Assessment a WHERE a.Id=AssessmentComponent.AssessmentId and a.Title=@t1 and a.DateCreated=@d1 ", con);
                cmd1.Parameters.AddWithValue("@t1", textBox1.Text);
                cmd1.Parameters.AddWithValue("@d1", Convert.ToDateTime(dateTimePicker1.Text));
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE Assessment WHERE Title=@t2 and DateCreated=@d2 ", con);
                cmd2.Parameters.AddWithValue("@t2", textBox1.Text);
                cmd2.Parameters.AddWithValue("@d2", Convert.ToDateTime(dateTimePicker1.Text));
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form5 form = new Form5();
            form.Show();
        }
    }
}
