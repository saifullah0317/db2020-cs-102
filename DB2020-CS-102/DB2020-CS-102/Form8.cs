﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form8 form = new Form8();
            form.Show();
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Clo ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                comboBox1.Items.Add(dr["Id"].ToString());
            }
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Rubric", con2);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView1.DataSource = dt2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Rubric values(@Details,@CloId,)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Details", textBox1.Text);
            cmd.Parameters.AddWithValue("@CloId", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric Details Added Successfully!!!!!");
            */
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Rubric values(@Id, @Details,@CloId)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", textBox3.Text);
            cmd.Parameters.AddWithValue("@Details", textBox1.Text);
            cmd.Parameters.AddWithValue("@CloId", comboBox1.Text);


            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric Successfully Added!!!!!!!!!!");
            this.Hide();
            Form8 form = new Form8();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update Rubric set Details=@detail,CloId=@cloid where Id=@id", con);
            cmd.Parameters.AddWithValue("@id", textBox3.Text);
            cmd.Parameters.AddWithValue("@detail", textBox1.Text);
            cmd.Parameters.AddWithValue("@cloid", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric Updated Successfully");
            this.Hide();
            Form8 form = new Form8();
            form.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric where Id=@id", con);
            cmd.Parameters.AddWithValue("@id", textBox2.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            
            /*
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from Rubric where Id=@id", con2);
            cmd2.Parameters.AddWithValue("@id", textBox2.Text);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView1.DataSource = dt2;
            */
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details,CloId from Rubric where Id=@id", con);
            cmd.Parameters.AddWithValue("@id", textBox3.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox1.Text = da.GetValue(0).ToString();
                comboBox1.Text = da.GetValue(1).ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (Rubric levels, Assessment components,Students' evaluation, etc.) regarding this Rubric will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From Rubric r,RubricLevel rl,AssessmentComponent ac WHERE ((r.Id=rl.RubricId and rl.Id=StudentResult.RubricMeasurementId) or (r.Id=ac.RubricId and ac.Id=StudentResult.AssessmentComponentId)) and r.Id=@id ", con);
                cmd.Parameters.AddWithValue("@id", int.Parse(textBox3.Text));
                cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("DELETE RubricLevel From Rubric r WHERE r.Id=RubricLevel.RubricId and r.Id=@id1 ", con);
                cmd1.Parameters.AddWithValue("@id1", int.Parse(textBox3.Text));
                cmd1.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("DELETE AssessmentComponent From Rubric r WHERE r.Id=AssessmentComponent.RubricId and r.Id=@id2 ", con);
                cmd3.Parameters.AddWithValue("@id2", int.Parse(textBox3.Text));
                cmd3.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE Rubric WHERE Id=@id3 ", con);
                cmd2.Parameters.AddWithValue("@id3", int.Parse(textBox3.Text));
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form8 form = new Form8();
            form.Show();
        }
    }
}
