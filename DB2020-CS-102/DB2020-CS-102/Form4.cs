﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select s.Id,s.FirstName,s.LastName,s.Contact,s.Email,s.RegistrationNumber,l.Name from Student s,Lookup l where s.Status=l.LookupId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            comboBox1.Items.Clear();
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select LookupId from Lookup where Category='STUDENT_STATUS' ", con1);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            foreach (DataRow dr in dt1.Rows)
            {
                comboBox1.Items.Add(dr["LookupId"].ToString());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber, @Status)", con);
            cmd.Parameters.AddWithValue("@FirstName", textBox2.Text);
            cmd.Parameters.AddWithValue("@LastName", textBox3.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
            cmd.Parameters.AddWithValue("@Email", textBox5.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", textBox1.Text);
            cmd.Parameters.AddWithValue("@Status", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            this.Hide();
            Form4 form = new Form4();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 form = new Form4();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update Student set FirstName=@fName,LastName=@lName,Contact=@contact,Email=@email,Status=@status where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox1.Text);
            cmd.Parameters.AddWithValue("@fName", textBox2.Text);
            cmd.Parameters.AddWithValue("@lName", textBox3.Text);
            cmd.Parameters.AddWithValue("@contact", textBox4.Text);
            cmd.Parameters.AddWithValue("@email", textBox5.Text);
            cmd.Parameters.AddWithValue("@status", comboBox1.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Updated Successfully");
            this.Hide();
            Form4 form = new Form4();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select FirstName,LastName,Contact,Email,Status from Student where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox2.Text = da.GetValue(0).ToString();
                textBox3.Text = da.GetValue(1).ToString();
                textBox4.Text = da.GetValue(2).ToString();
                textBox5.Text = da.GetValue(3).ToString();
                comboBox1.Text = da.GetValue(4).ToString();
            }
            if (textBox1.Text == "")
            {
                textBox1.Clear();
                textBox3.Clear();
                textBox4.Clear();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox7.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (attendance, results, etc.) regarding this Student will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From Student s WHERE s.Id=StudentResult.StudentId and s.RegistrationNumber=@reg ", con);
                cmd.Parameters.AddWithValue("@reg", textBox1.Text);
                cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("DELETE StudentAttendance From Student s WHERE s.Id=StudentAttendance.StudentId and s.RegistrationNumber=@reg ", con);
                cmd1.Parameters.AddWithValue("@reg", textBox1.Text);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE Student WHERE RegistrationNumber=@reg1 ", con);
                cmd2.Parameters.AddWithValue("@reg1", textBox1.Text);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form4 form = new Form4();
            form.Show();
        }
    }
}
