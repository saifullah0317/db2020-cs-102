﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form11 form = new Form11();
            form.Show();
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Student ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                comboBox1.Items.Add(dr["Id"].ToString());
            }
            comboBox2.Items.Clear();
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select Id from AssessmentComponent ", con1);
            cmd1.ExecuteNonQuery();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            foreach (DataRow dr in dt1.Rows)
            {
                comboBox2.Items.Add(dr["Id"].ToString());
            }
            comboBox3.Items.Clear();
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select Id from RubricLevel ", con2);
            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            foreach (DataRow dr in dt2.Rows)
            {

                comboBox3.Items.Add(dr["Id"].ToString());
            }
            
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("select s.RegistrationNumber,a.Title as Assessment,ac.Name as Component,sr.EvaluationDate,((select rl1.MeasurementLevel from RubricLevel rl1 where sr.RubricMeasurementId=rl1.Id)*(select ac1.TotalMarks from AssessmentComponent ac1 where sr.AssessmentComponentId=ac1.Id)/(select max(rl1.MeasurementLevel) from AssessmentComponent ac1,RubricLevel rl1,Rubric r1 where sr.AssessmentComponentId=ac1.Id and ac1.RubricId=r1.Id and rl1.RubricId=r1.Id group by r1.Id)) as [Obtained Marks] from StudentResult sr,Student s,AssessmentComponent ac,Assessment a where sr.StudentId=s.Id and sr.AssessmentComponentId=ac.Id and ac.AssessmentId=a.Id", con3);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            
            /*
            dt3.Columns.Add("Obtained Marks", typeof(double));
            foreach (DataRow row in dt3.Rows)
            {
                //need to set value to NewColumn column
                row["NewColumn"] = ("select ");   // or set it to some other value
            }
            */
            dataGridView1.DataSource = dt3;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select AssessmentComponentId,RubricMeasurementId,EvaluationDate from StudentResult where StudentId=@stdId", con);
            cmd.Parameters.AddWithValue("@stdId", int.Parse(comboBox1.Text));
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                comboBox2.Text = da.GetValue(0).ToString();
                comboBox3.Text = da.GetValue(1).ToString();
                dateTimePicker1.Text = da.GetValue(2).ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update StudentResult set AssessmentComponentId=@ass,RubricMeasurementId=@rubric,EvaluationDate=@date where StudentId=@stdId", con);
            cmd.Parameters.AddWithValue("@stdId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@ass", int.Parse(comboBox2.Text));
            cmd.Parameters.AddWithValue("@rubric", int.Parse(comboBox3.Text));
            cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Student result Updated Successfully");
            this.Hide();
            Form11 form = new Form11();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();            
            SqlCommand cmd = new SqlCommand("INSERT  INTO  StudentResult values(@StudentId,@AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@AssessmentComponentId", int.Parse(comboBox2.Text));
            cmd.Parameters.AddWithValue("@RubricMeasurementId", int.Parse(comboBox3.Text));
            cmd.Parameters.AddWithValue("@EvaluationDate", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Student Result Added Successfully!!!!!");
            this.Hide();
            Form11 form = new Form11();
            form.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from StudentResult where StudentId=@id", con3);
            cmd3.Parameters.AddWithValue("@id", textBox2.Text);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE StudentResult WHERE StudentId=@id and AssessmentComponentId=@aId ", con);
            cmd.Parameters.AddWithValue("@id", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@aId", int.Parse(comboBox2.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Deleted Successfully!");
            this.Hide();
            Form11 form = new Form11();
            form.Show();
        }
    }
}
