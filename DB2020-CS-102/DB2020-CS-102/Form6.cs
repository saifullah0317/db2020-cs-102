﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (Rubrics, Rubric levels, Assessment components,Students' evaluation, etc.) regarding this Rubric will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From Clo c,Rubric r,RubricLevel rl,AssessmentComponent ac WHERE (c.Id=r.CloId and r.Id=rl.RubricId and rl.Id=StudentResult.RubricMeasurementId and c.Name=@name) or (c.Id=r.CloId and r.Id=ac.RubricId and ac.Id=StudentResult.AssessmentComponentId and c.Name=@name) ", con);
                cmd.Parameters.AddWithValue("@name", textBox1.Text);
                cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("DELETE RubricLevel From Clo c,Rubric r WHERE c.Id=r.CloId and r.Id=RubricLevel.RubricId and c.Name=@name1 ", con);
                cmd1.Parameters.AddWithValue("@name1", textBox1.Text);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("DELETE AssessmentComponent From Clo c,Rubric r WHERE c.Id=r.CloId and r.Id=AssessmentComponent.RubricId and c.Name=@name2 ", con);
                cmd3.Parameters.AddWithValue("@name2", textBox1.Text);
                cmd3.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE Rubric From Clo c WHERE c.Id=Rubric.CloId and c.Name=@name3 ", con);
                cmd2.Parameters.AddWithValue("@name3", textBox1.Text);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd4 = new SqlCommand("DELETE Clo WHERE Name=@name4 ", con);
                cmd4.Parameters.AddWithValue("@name4", textBox1.Text);
                cmd4.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form6 form = new Form6();
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update Clo set DateCreated=@dateC,DateUpdated=@dateU where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            cmd.Parameters.AddWithValue("@dateC", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@dateU", Convert.ToDateTime(dateTimePicker2.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Clo Updated Successfully");
            this.Hide();
            Form6 form = new Form6();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Clo values(@Name,@DateCreated,@DateUpdated)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Name", textBox1.Text);
            cmd.Parameters.AddWithValue("@DateCreated", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@DateUpdated", Convert.ToDateTime(dateTimePicker2.Text));


            cmd.ExecuteNonQuery();
            MessageBox.Show("CLO Details Added Successfully!!!!!");
            this.Hide();
            Form6 form = new Form6();
            form.Show();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 form = new Form6();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DateCreated,DateUpdated from Clo where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                dateTimePicker1.Text = da.GetValue(0).ToString();
                dateTimePicker2.Text = da.GetValue(1).ToString();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox2.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
