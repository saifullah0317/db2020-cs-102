﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form9 form = new Form9();
            form.Show();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());
            }
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from RubricLevel", con3);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId,MeasurementLevel from RubricLevel where Details=@detail", con);
            cmd.Parameters.AddWithValue("detail", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                comboBox1.Text = da.GetValue(0).ToString();
                textBox2.Text = da.GetValue(1).ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  RubricLevel values(@RubricId,@Details,@MeasurementLevel)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@Details", textBox1.Text);
            cmd.Parameters.AddWithValue("@MeasurementLevel", int.Parse(textBox2.Text));


            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric-level Details Added Successfully!!!!!");
            this.Hide();
            Form9 form = new Form9();
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update RubricLevel set RubricId=@rubric,MeasurementLevel=@ml where Details=@detail", con);
            cmd.Parameters.AddWithValue("@rubric", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@detail", textBox1.Text);
            cmd.Parameters.AddWithValue("@ml", int.Parse(textBox2.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric-level details Updated Successfully");
            this.Hide();
            Form9 form = new Form9();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from RubricLevel where Details=@detail", con3);
            cmd3.Parameters.AddWithValue("@detail", textBox3.Text);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (like students' evaluation) regarding this Rubric level will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From RubricLevel r WHERE r.Id=StudentResult.RubricMeasurementId and r.RubricId=@ri and r.Details=@rd ", con);
                cmd.Parameters.AddWithValue("@rd", textBox1.Text);
                cmd.Parameters.AddWithValue("@ri", comboBox1.Text);
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE RubricLevel WHERE RubricId=@ri1 and Details=@rd1 ", con);
                cmd2.Parameters.AddWithValue("@rd1", textBox1.Text);
                cmd2.Parameters.AddWithValue("@ri1", comboBox1.Text);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form9 form = new Form9();
            form.Show();
        }
    }
}
