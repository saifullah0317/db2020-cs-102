﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
            {
                this.Hide();
                Form4 form = new Form4();
                form.Show();
            }
            else if(radioButton9.Checked)
            {
                this.Hide();
                Form5 form = new Form5();
                form.Show();
            }
            else if (radioButton6.Checked)
            {
                this.Hide();
                Form6 form = new Form6();
                form.Show();
            }
            else if (radioButton7.Checked)
            {
                this.Hide();
                Form8 form = new Form8();
                form.Show();
            }
            else if (radioButton10.Checked)
            {
                this.Hide();
                Form7 form = new Form7();
                form.Show();
            }
            else if (radioButton8.Checked)
            {
                this.Hide();
                Form9 form = new Form9();
                form.Show();
            }
            /*
            else if (radioButton5.Checked)
            {
                this.Hide();
                Form10 form = new Form10();
                form.Show();
            }
            */
            else if (radioButton2.Checked)
            {
                this.Hide();
                Form11 form = new Form11();
                form.Show();
            }
            else if (radioButton3.Checked)
            {
                this.Hide();
                Form12 form = new Form12();
                form.Show();
            }
            else if (radioButton4.Checked)
            {
                this.Hide();
                Form13 form = new Form13();
                form.Show();
            }
            /*
            else if (radioButton12.Checked)
            {
                Environment.Exit(0);
            }
            */
            else if (radioButton11.Checked)
            {
                this.Hide();
                Form14 form = new Form14();
                form.Show();
            }

        }
        /*
        private void button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 form = new Form1();
            form.Show();
        }
        */
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
