﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form13 : Form
    {
        public Form13()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form13 form = new Form13();
            form.Show();
        }

        private void Form13_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.Id from Student s,Lookup l where l.LookupId=s.Status and l.Name<>'InActive'", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());
            }
            comboBox2.Items.Clear();

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select Id from ClassAttendance ", con1);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            foreach (DataRow dr in dt1.Rows)
            {
                comboBox2.Items.Add(dr["Id"].ToString());
            }
            comboBox3.Items.Clear();

            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select LookupId from Lookup where Category='ATTENDANCE_STATUS'", con2);
            cmd2.ExecuteNonQuery();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            foreach (DataRow dr in dt2.Rows)
            {
                comboBox3.Items.Add(dr["LookupId"].ToString());
            }
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("select ca.AttendanceDate,s.RegistrationNumber,l.Name from StudentAttendance sa,ClassAttendance ca,Student s,Lookup l where sa.AttendanceId=ca.Id and sa.StudentId=s.Id and l.LookupId=sa.AttendanceStatus", con3);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT  INTO  StudentAttendance values(@AttendanceId,@StudentId,@AttendanceStatus)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@AttendanceId", int.Parse(comboBox2.Text));
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@AttendanceStatus", int.Parse(comboBox3.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Student Attendance Added Successfully!!!!!");
            this.Hide();
            Form13 form = new Form13();
            form.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select AttendanceId,AttendanceStatus from StudentAttendance where StudentId=@stdId", con);
            cmd.Parameters.AddWithValue("@stdId", int.Parse(comboBox1.Text));
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                comboBox2.Text = da.GetValue(0).ToString();
                comboBox3.Text = da.GetValue(1).ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update StudentAttendance set AttendanceId=@ai,AttendanceStatus=@as where StudentId=@stdId", con);
            cmd.Parameters.AddWithValue("@stdId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@ai", int.Parse(comboBox2.Text));
            cmd.Parameters.AddWithValue("@as", int.Parse(comboBox3.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Attendance Updated Successfully");
            this.Hide();
            Form13 form = new Form13();
            form.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from StudentAttendance where StudentId=@stdId", con3);
            cmd3.Parameters.AddWithValue("@stdId", int.Parse(comboBox1.Text));
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE StudentAttendance WHERE StudentId=@id and AttendanceId=@aId ", con);
            cmd.Parameters.AddWithValue("@id", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@aId", int.Parse(comboBox2.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Deleted Successfully!");
            this.Hide();
            Form13 form = new Form13();
            form.Show();
        }
    }
}
