﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_102
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());
            }
            comboBox2.Items.Clear();

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select Id from Assessment ", con1);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            foreach (DataRow dr in dt1.Rows)
            {
                comboBox2.Items.Add(dr["Id"].ToString());
            }
            var con3 = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from AssessmentComponent", con3);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView1.DataSource = dt3;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT  INTO  AssessmentComponent values(@Name,@RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Name", textBox1.Text);
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@TotalMarks", textBox2.Text);
            cmd.Parameters.AddWithValue("@DateCreated", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@DateUpdated", Convert.ToDateTime(dateTimePicker2.Text)); ;
            cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(comboBox2.Text));


            cmd.ExecuteNonQuery();
            MessageBox.Show("Component Details Added Successfully!!!!!");
            this.Hide();
            Form7 form = new Form7();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId,TotalMarks,DateCreated,DateUpdated,AssessmentId from AssessmentComponent where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                comboBox1.Text = da.GetValue(0).ToString();
                textBox2.Text = da.GetValue(1).ToString();
                dateTimePicker1.Text = da.GetValue(2).ToString();
                dateTimePicker2.Text = da.GetValue(3).ToString();
                comboBox2.Text = da.GetValue(4).ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("update AssessmentComponent set RubricId=@rubric,DateCreated=@dateC,TotalMarks=@marks,DateUpdated=@dateU,AssessmentId=@ass where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            cmd.Parameters.AddWithValue("@dateC", Convert.ToDateTime(dateTimePicker1.Text));
            cmd.Parameters.AddWithValue("@dateU", Convert.ToDateTime(dateTimePicker2.Text));
            cmd.Parameters.AddWithValue("@marks", int.Parse(textBox2.Text));
            cmd.Parameters.AddWithValue("@rubric", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@ass", int.Parse(comboBox2.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Assessments Updated Successfully");
            this.Hide();
            Form7 form = new Form7();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 form = new Form2();
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form7 form = new Form7();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox3.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("All the data (like Students' evaluation) regarding this Assessment component will be deleted! Do you want?", "Warning", buttons);
            if (result == DialogResult.Yes)
            {
                SqlCommand cmd = new SqlCommand("DELETE StudentResult From AssessmentComponent a WHERE a.Id=StudentResult.AssessmentComponentId and a.Name=@name ", con);
                cmd.Parameters.AddWithValue("@name", textBox1.Text);
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE AssessmentComponent WHERE Name=@name1 ", con);
                cmd2.Parameters.AddWithValue("@name1", textBox1.Text);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Successfully!");
            }
            this.Hide();
            Form7 form = new Form7();
            form.Show();
        }
    }
}
